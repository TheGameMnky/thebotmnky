// config files
const config = require('./config.json');
const twitch = require('./twitch.json');

// frameworks and setup
const Discord = require('discord.js');
const request = require('request');
const client = new Discord.Client();
const fs = require('fs');

// variables
var Live = [];
var servers = {};
var oldFollow = 0;
var newFollow = 0;
var date = null;

function checkForLive() {
  console.log("checking twitch");
  var channel = "379221517696761856";
    twitch.forEach(function (channelID) {
        var twitchChannel = channelID.twitchID
        request({
            uri: 'https://api.twitch.tv/kraken/streams/' + twitchChannel,
            json: true,
            headers: {
                'Client-ID': "o89grp7uggp71srksvx2b3fqisgxff"
            }
        }, function (error, response, body) {
            if (!error && response.statusCode === 200 && body.stream !== null) {
                if (Live.indexOf(twitchChannel) === -1) {
                    Live.push(twitchChannel)
                    var embed = {
                        author: {
                            name: twitchChannel,
                            icon_url: body.stream.channel.logo,
                            url: "https://twitch.tv/" + twitchChannel
                        },
                        title: body.stream.channel.url,
                        url: "https://twitch.tv/" + twitchChannel,
                        thumbnail: {
                            url: body.stream.channel.logo
                        },
                        fields: [{
                                name: "Stream Title",
                                value: body.stream.channel.status,
                                inline: false
                            },{
                                name: "Played Game",
                                value: body.stream.game,
                                inline: false
                            },
                            {
                                name: "Viewers",
                                value: body.stream.viewers,
                                inline: true
                            },
                            {
                                name: "Followers",
                                value: body.stream.channel.followers,
                                inline: true
                            }
                        ],
                        image: { 
                            url: `${body.stream.preview.large}?${Math.floor(Math.random() * 100) + 1}`,
                        },
                        footer: {
                            text: "Twitch.tv"
                        }
                    }
                    client.channels.get(channel).send(
                        "@everyone " + twitchChannel + " is now live on <https://twitch.tv/" + twitchChannel + "> You should definately go check them out they are an awesome person and an awesome streamer!",
                        { embed: embed }
                        );
                    console.log("Printed to discord.");
                }
            } else if (!error && response.statusCode === 200 && body.stream === null){
                if (Live.indexOf(twitchChannel) !== -1){
                    Live.splice(Live.indexOf(twitchChannel), 1);
                    console.log(twitchChannel + "is no longer Live");
                } else {

                }
            } else {
                console.log('Error: ', error); // Log the error
                console.log("erroring at twitch")
            }
        });
        
    }, this);
}

function weeklyTwitchStatus(){
    try {
        request({
            uri: 'https://api.twitch.tv/kraken/channels/ExoticCollector/follows',
            json: true,
            headers: {
                'Client-ID': "o89grp7uggp71srksvx2b3fqisgxff"
            }
        }, function (error, response, body) {
            newFollow = body._total;
            var followDiff = newFollow - oldFollow;
            client.users.get("343309539401269248").send(`New Followers: ${followDiff}! \nCurrent Followers: ${newFollow}!`);
            oldFollow = newFollow;
        });
    } catch (e) {
        console.log(e);
    }
}

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);

    checkForLive();
    weeklyTwitchStatus();

    var timer = setInterval(function (){
        checkForLive();
    }, 600000);

    var timer2 = setInterval(function (){
        weeklyTwitchStatus();
    }, 86400000);
});

client.on('guildMemberAdd', member => {
    client.channels.get("379221725277323266").send(`\n\n\n\nWelcome, ${member}. \`\`\`Please read the #rules channel any broken rules will end in a ban. We welcome all please feel free to join any conversation and welcome to the family.\`\`\`\n Make sure to follow at https://twitch.tv/ExoticCollector \n\n\n\`\`\`Make sure you check out all the people in the twitch announcement channel they are all amazing people and deserve your support!\`\`\``);
    if (member.user.bot === true){
        member.addRole("367345274026328076");
    }
});

client.on('message', message => {
    if (message.content.indexOf(config.cmdPrefix) !== 0) return;
    else { // Execute command!
        var args = message.content.substring(config.cmdPrefix.length).split(" ");
        if (message.channel.id === "400944774959464448") {
            switch (args[0].toLowerCase()){
                case "clear":
                    const admins = config.admins;
                    admins.forEach(function(item){
                        if (message.author.id === item){
                            if(isNaN(args[1])) {
                                return message.reply("That Does Not Seem To Be A Valid Number!");
                            }
                            if (args[1] >= 2 || args[1] <= 100){
                                message.channel.bulkDelete(args[1]);
                            }
                        }
                    });
                    break;
                case "ping":
                    message.reply('pong');
                    break;
                case "twitch":
                    checkForLive();
                    break;
                case "avatar":
                    message.reply(message.author.avatarURL);
                    break;
                case "v":
                    message.reply("Current Bot Version Is V0.0.4 (Crash Fix Update)");
                    break;
                case "help":
                    var embed = {
                        title: "HELP IS HERE DON'T WORRY YOUNG PADOWAN!!",
                        description: "Commands are !help but you know that \n !v, !avatar, !ping.",
                        footer: {
                            text: "Thanks :)"
                        }
                    }
                    message.author.send({ embed: embed });
                    break;
                default:
                    message.reply("That Is Not A Command");
                    break;
            }
        } else {
            if (message.channel.id === "378191044346642442") {
                switch(args[0].toLowerCase()) {
                    case "clear":
                        const admins = config.admins;
                        admins.forEach(function(item){
                            if (message.author.id === item){
                                if(isNaN(args[1])) {
                                    return message.reply("That Does Not Seem To Be A Valid Number!");
                                }
                                if (args[1] >= 2 || args[1] <= 100){
                                    message.channel.bulkDelete(args[1]);
                                }
                            }
                        });
                        break;
                    
                    default:
                        message.reply("That Is Not A Command");
                        break;
                }
            } else {
                switch(args[0].toLowerCase()) {
                    case "clear":
                        const admins = config.admins;
                        admins.forEach(function(item){
                            if (message.author.id === item){
                                if(isNaN(args[1])) {
                                    return message.reply("That Does Not Seem To Be A Valid Number!");
                                }
                                if (args[1] >= 2 || args[1] <= 100){
                                    message.channel.bulkDelete(args[1]);
                                }
                            }
                        });
                        break;
                    case "ping":
                    case "v":
                    case "avatar":
                    case "help":
                        var embed = {
                            title: "You Did It Wrong!",
                            description: "Please Join The Music Channel If You Are Trying To Use Music Commands And Use The Channel That Pops Up. \n For Any Other Commands Use The Bot Command Channel Under General.",
                            footer: {
                                text: "Thanks :)"
                            }
                        }
                        message.author.send({ embed: embed });
                        break;
                    default:
                        message.reply("That Is Not A Command");
                        break;
                }
            }
        }
    }   
});

client.login(config.token);